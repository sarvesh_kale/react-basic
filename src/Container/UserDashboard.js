import React, { Component } from 'react'
import UserForm from './UserForm'
import UserTable from './UserTable'
class UserDashboard extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedUser: {
                name: '',
                dob: '',
                email: '',
                phoneNumber: '',
                gender: '',
                description: '',
                age: '',
                editId: '',
            },
            users: [],
        };
    }

    handleResetState = () => {
        let selectedUserCopy = {}
        selectedUserCopy.name = ''
        selectedUserCopy.dob = ''
        selectedUserCopy.email = ''
        selectedUserCopy.phoneNumber = ''
        selectedUserCopy.gender = ''
        selectedUserCopy.description = ''
        selectedUserCopy.age = ''
        selectedUserCopy.editId = ''
        this.setState({
            selectedUser: selectedUserCopy
        });
    }

    handleSave = (event) => {
        event.preventDefault()
        let temp = {}
        temp.id = parseInt(this.state.editId) + 1
        temp.name = this.state.selectedUser.name
        temp.dob = this.state.selectedUser.dob
        temp.email = this.state.selectedUser.email
        temp.phoneNumber = this.state.selectedUser.phoneNumber
        temp.gender = this.state.selectedUser.gender
        temp.description = this.state.selectedUser.description
        temp.age = this.state.selectedUser.age
        let usersCopy = [...this.state.users]
        usersCopy[parseInt(this.state.selectedUser.editId)] = temp
        this.setState({
            users: usersCopy
        })
        this.handleResetState();
    }


    handleDlelteRow = (user) => {
        this.state.users.splice(user.target.value, 1);
        this.setState(this.state.users);
    }

    handleFormSubmit = (event) => {
        let count = this.state.users.length + 1
        let temp = {}
        temp.id = count
        temp.name = this.state.selectedUser.name
        temp.dob = this.state.selectedUser.dob
        temp.email = this.state.selectedUser.email
        temp.phoneNumber = this.state.selectedUser.phoneNumber
        temp.gender = this.state.selectedUser.gender
        temp.description = this.state.selectedUser.description
        temp.age = this.state.selectedUser.age
        this.setState({
            users: [...this.state.users, temp]
        })
        this.handleResetState();
        event.preventDefault();
    }

    handleDescription = (event) => {
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.description = event.target.value
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handleGender = (event) => {
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.gender = event.target.value
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handlePhoneNumber = (event) => {
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.phoneNumber = event.target.value
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handleEmail = (event) => {
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.email = event.target.value
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handleName = (event) => {
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.name = event.target.value
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handleAge = (event) => {
        let currentYear = new Date().getFullYear()
        let yearOfBirth = parseInt(event.target.value.split("-")[0])
        let selectedUserCopy = { ...this.state.selectedUser }
        selectedUserCopy.dob = event.target.value
        selectedUserCopy.age = currentYear - yearOfBirth
        this.setState({
            selectedUser: selectedUserCopy
        })
    }

    handleEditRow = (event) => {
        let value = this.state.users[event.target.value]
        value.editId = event.target.value
        console.log(value)
        this.setState({
            selectedUser: value
        })
    }

    render() {
        return (
            <div className="SplitPane">
                <div className="SplitPane-left">
                    <UserForm
                        handleDescription={this.handleDescription}
                        handleGender={this.handleGender}
                        handlePhoneNumber={this.handlePhoneNumber}
                        handleEmail={this.handleEmail}
                        handleName={this.handleName}
                        handleSubmit={this.handleFormSubmit}
                        handleAge={this.handleAge}
                        handleResetState={this.handleResetState}
                        handleSave={this.handleSave}
                        selectedUser = {this.state.selectedUser}
                    />
                </div>
                <div className="SplitPane-right">
                    <UserTable
                        handleEditRow={this.handleEditRow}
                        users={this.state.users}
                        handleDlelteRow={this.handleDlelteRow}
                    />
                </div>
            </div>
        );
    }

}

export default UserDashboard
