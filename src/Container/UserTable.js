import React from 'react'
import '../components/table.css'
import UserRow from '../components/UserRow'

const Table = props => {
    let users = props.users.map(
        (user, index) => {
            return (
                <UserRow
                    users={user}
                    index={index}
                    onRowDel={props.handleDlelteRow}
                    onRowEdit={props.handleEditRow}
                />)
        }
    )
    return (
        <div>
            <div>
                <h1>Employee Table</h1>
            </div>
            <table className="myTable">
                <thead>
                    <tr>
                        <th>Sr.no</th>
                        <th>Name</th>
                        <th>Age</th>
                        <th>Gender</th>
                        <th>Email</th>
                        <th>Delete Row</th>
                        <th>Edit Row</th>
                    </tr>
                </thead>
                <tbody id="tbody">
                    {users}
                </tbody>
            </table>
        </div>
    )
}

export default Table
