import React from 'react'
import Input from '../components/Input'
import TextArea from '../components/TextArea'
import Select from '../components/Select'
import Button from '../components/Button'

const UserForm = props => {
    return (
        <form onSubmit={props.handleSubmit} className="form">
            <div>
                <h1>Employee form</h1>
            </div>
            <Input
                title="Name: "
                placeholder="Enter your name"
                type="text"
                pattern="[a-zA-Z\s]+"
                handleEvent={props.handleName}
                value={props.selectedUser.name}
            />
            <Input
                title="Date of birth: "
                placeholder="Enter your Date of birth"
                type="date" handleEvent={props.handleAge}
                value={props.selectedUser.dob}
            />
            <Input
                title="Email: "
                placeholder="Enter your email address"
                type="email"
                handleEvent={props.handleEmail}
                value={props.selectedUser.email}
            />
            <Input
                title="Phone Number: "
                placeholder="Enter your phone number"
                type="number"
                handleEvent={props.handlePhoneNumber}
                value={props.selectedUser.phoneNumber}
            />
            <Select
                title="Gender"
                name="gender"
                placeholder="Select Gender"
                handleEvent={props.handleGender}
                value={props.selectedUser.gender}
            />
            <TextArea
                title="Description: "
                placeholder="Describe yourself."
                rows="5" cols="70" id="Description"
                handleEvent={props.handleDescription}
                value={props.selectedUser.description}
            />
            <div className="float-left">
                <Button
                    type="submit"
                    title="Submit"

                />
            </div>
            <div className="float-right">
                <Button
                    title="Save"
                    action={props.handleSave}
                />
            </div>

        </form>
    )
}

export default UserForm
