import React, { Component } from 'react';
import './App.css';
import UserDashboard from './Container/UserDashboard'



class App extends Component {

  render() {

    return (
      <div className="App">
        <header className="App-header">
          <h1>My Org</h1>
        </header>
        <UserDashboard />
      </div>
    );
  }
}
export default App;
