import React from 'react';
import './button.css';

const Button = props => {
    return (
        <div>
            <button
                className={props.type === "primary" ? "btn btn-primary" : "btn btn-secondary"}
                onClick={props.action}
                value={props.index}
            >
                {props.title}
            </button>
        </div>
    )
}

export default Button;
