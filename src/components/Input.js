import React from 'react';
import './input.css';

const Input = props => {
    return (
        <div>
            <label>{props.title}</label>
            <input
                value={props.value}
                placeholder={props.placeholder}
                type={props.type}
                onChange={props.handleEvent}
                pattern={props.pattern}
                required
            />
        </div>
    )
}

export default Input;
