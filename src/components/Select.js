import React from 'react';

const Select = props => {
  return (
    <div>
      <label htmlFor={props.name}> {props.title} </label>
      <select onChange={props.handleEvent} required>
        <option value="" selected>{props.placeholder}</option>
        <option value="Male">Male</option>
        <option value="Female">Female</option>
      </select>
    </div>
  );
};

export default Select;
