import React from 'react';
import Button from './Button';

const UserRow = props => {
    return (
        <tr>
            <td>{props.users.id}</td>
            <td>{props.users.name}</td>
            <td>{props.users.age}</td>
            <td>{props.users.gender}</td>
            <td>{props.users.email}</td>
            <td><Button title="Delete" index={props.index} action={props.onRowDel} /></td>
            <td><Button title="Edit" index={props.index} action={props.onRowEdit} /></td>
        </tr>
    )
}
export default UserRow;