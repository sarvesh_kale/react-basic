import React from 'react'

const TextArea = props => {
    return (
        <div>
            <label for={props.id}>{props.title}</label>
            <textarea
                value={props.value}
                id={props.id}
                rows={props.rows}
                cols={props.cols}
                placeholder={props.placeholder}
                onChange={props.handleEvent}
                required
            />
        </div>
    )
}


export default TextArea